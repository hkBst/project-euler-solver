use std::{cell::RefCell, ops::RangeFrom, rc::Rc};

pub fn factorial(n: usize) -> usize {
    (2..=n).product()
}

pub fn divides(d: usize, n: usize) -> bool {
    n % d == 0
}

pub fn gcd(mut a: usize, mut b: usize) -> usize {
    if b > a {
        std::mem::swap(&mut a, &mut b);
    }
    while b != 0 {
        a %= b;
        std::mem::swap(&mut a, &mut b);
    }
    a
}

pub fn lcm(a: usize, b: usize) -> usize {
    a * b / gcd(a, b)
}

// triangle number is sum_{n=1}^N n = n*n/2 +n/2 = n(n+1)/2
// n and n+1 share no non-unit factor!
pub fn triangle_nums() -> impl Iterator<Item = usize> {
    (1..).scan(0, |state, n| {
        *state += n;
        Some(*state)
    })
}

pub fn divisor_sum(mut n: usize) -> usize {
    let mut res = 1;
    let mut k = 2;
    while k * k <= n {
        let mut p = 1;
        while divides(k, n) {
            p = p * k + 1;
            n /= k;
        }
        res *= p;
        k += 1;
    }
    if n > 1 {
        res *= n + 1;
    }
    res
}

pub struct Primes {
    nums: RefCell<RangeFrom<usize>>,
    cache: Rc<RefCell<Vec<usize>>>,
}

impl Primes {
    pub fn new() -> Self {
        Primes {
            nums: RefCell::new(2..), // FIXME, can we do slightly better with 2,3,5, ..., 2n+1 ?
            cache: Rc::new(RefCell::new(Vec::new())),
        }
    }

    pub fn index(&self, n: usize) -> usize {
        self.cache_up_to(n);
        self.cache.borrow()[n].to_owned()
    }

    fn cache_next(&self) {
        let next_prime = (*self.nums.borrow_mut())
            .find(|&n| {
                self.cache
                    .borrow()
                    .iter()
                    .take_while(|&p| p * p <= n)
                    .all(|&p| !divides(p, n))
            })
            .unwrap();
        self.cache.borrow_mut().push(next_prime);
    }

    fn cache_up_to(&self, n: usize) {
        let capacity = self.cache.borrow().capacity();
        if n >= capacity {
            self.cache.borrow_mut().reserve(n - capacity);
        }
        while n >= self.cache.borrow().len() {
            self.cache_next();
        }
    }

    pub fn iter(&self) -> PrimesIter {
        PrimesIter { primes: self, n: 0 }
    }

    pub fn factors_iter(&self, n: usize) -> FactorIter {
        FactorIter {
            primes: self.iter(),
            n,
        }
    }

    pub fn num_divisors(&self, n: usize) -> usize {
        if n == 0 {
            return 0;
        }
        self.factors_iter(n)
            .fold(1, |num_d, (_p, num_p)| num_d * (num_p + 1))
    }

    // if you have prime factorization, then you can enumerate the divisors by taking products of increasing numbers of prime factors
    // d(2*2*3) = (1+2+4)*(1+3)= 1 + 2 + 4 + 3 + 6 + 12
    // (sum_{i=0}^{i=num_p} p^i) * (p-1) = p^(num_p+1)-1
    pub fn divisor_sum(&self, n: usize) -> usize {
        self.factors_iter(n).fold(1, |res, (p, num_p)| {
            res * (p.pow(num_p as u32 + 1) - 1) / (p - 1)
            //res * (1..=num_p).fold(1, |res, _i| res * p + 1) // slightly faster alternative?
        })
    }

    pub fn proper_divisor_sum(&self, n: usize) -> usize {
        self.divisor_sum(n) - n
    }

    pub fn triangle_num_factors_iter(&self) -> TriangleNumFactorIter {
        TriangleNumFactorIter {
            primes: self,
            n: 1,
            num_d: 1,
        }
    }
}

pub struct PrimesIter<'a> {
    primes: &'a Primes,
    n: usize,
}

impl<'a> Iterator for PrimesIter<'a> {
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        let res = self.primes.index(self.n);
        self.n += 1;
        Some(res)
    }

    // fn advance_by(&mut self, n: usize) -> Result<(), usize> {
    //     self.primes.cache_up_to(n);
    //     self.n += n;
    //     Ok(())
    // }
}

pub struct FactorIter<'a> {
    primes: PrimesIter<'a>,
    n: usize,
}

impl<'a> Iterator for FactorIter<'a> {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        if self.n <= 1 {
            None
        } else {
            loop {
                let p = self.primes.next().unwrap();
                if p * p > self.n {
                    let n = self.n;
                    self.n = 1;
                    return Some((n, 1));
                }
                if divides(p, self.n) {
                    let mut num_p = 1;
                    self.n /= p;
                    while divides(p, self.n) {
                        num_p += 1;
                        self.n /= p;
                    }
                    return Some((p, num_p));
                }
            }
        }
    }
}

pub struct TriangleNumFactorIter<'a> {
    primes: &'a Primes,
    n: usize,
    num_d: usize,
}

// (1,1) (1,3) (3,2) (2,5) (5,3) (3,7) (7,4) (4,9)
impl<'a> TriangleNumFactorIter<'a> {
    fn reduced_num_divisors(&self, n: usize) -> usize {
        let mut divisors = self.primes.factors_iter(n);
        divisors
            .next()
            .map(|(p, num_p)| {
                divisors.fold(num_p + if p == 2 { 0 } else { 1 }, |num_d, (_p, num_p)| {
                    num_d * (num_p + 1)
                })
            })
            .unwrap()
    }
}

impl<'a> Iterator for TriangleNumFactorIter<'a> {
    type Item = (usize, usize);

    fn next(&mut self) -> Option<Self::Item> {
        self.n += 1;
        let num_d = self.reduced_num_divisors(self.n);
        let num_divs = self.num_d * num_d;
        self.num_d = num_d;
        Some((self.n * (self.n - 1) / 2, num_divs))
    }
}

pub struct Abundants<'a> {
    primes: &'a Primes,
    nums: RefCell<RangeFrom<usize>>,
    cache: Rc<RefCell<Vec<usize>>>,
}

impl<'a> Abundants<'a> {
    pub fn new(primes: &'a Primes) -> Self {
        Self {
            primes,
            nums: RefCell::new(12..),
            cache: Rc::new(RefCell::new(Vec::new())),
        }
    }

    pub fn index(&self, n: usize) -> usize {
        self.cache_up_to(n);
        self.cache.borrow()[n].to_owned()
    }

    fn cache_next(&self) {
        let next = (*self.nums.borrow_mut())
            .find(|&n| self.primes.proper_divisor_sum(n) > n)
            .unwrap();
        self.cache.borrow_mut().push(next);
    }

    fn cache_up_to(&self, n: usize) {
        let capacity = self.cache.borrow().capacity();
        if n >= capacity {
            self.cache.borrow_mut().reserve(n - capacity);
        }
        while n >= self.cache.borrow().len() {
            self.cache_next();
        }
    }

    pub fn iter(&self) -> AbundantsIter {
        AbundantsIter {
            abundants: self,
            n: 0,
        }
    }
}

pub struct AbundantsIter<'a> {
    abundants: &'a Abundants<'a>,
    n: usize,
}

impl<'a> Iterator for AbundantsIter<'a> {
    type Item = usize;

    fn next(&mut self) -> Option<usize> {
        let res = self.abundants.index(self.n);
        self.n += 1;
        Some(res)
    }

    // fn advance_by(&mut self, n: usize) -> Result<(), usize> {
    //     self.primes.cache_up_to(n);
    //     self.n += n;
    //     Ok(())
    // }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn factorial_test() {
        assert_eq!(factorial(0), 1);
        assert_eq!(factorial(1), 1);
        assert_eq!(factorial(2), 2);
        assert_eq!(factorial(3), 6);
        assert_eq!(factorial(4), 24);
    }

    #[test]
    fn divides_true() {
        (1..100_usize).for_each(|n| assert!(divides(n, n)));
    }

    #[test]
    fn divides_false() {
        (1..100_usize).for_each(|n| assert!(!divides(n + 1, n)));
    }

    #[test]
    fn swap_true() {
        let (mut a, mut b) = (1, 2);
        std::mem::swap(&mut a, &mut b);
        assert_eq!((a, b), (2, 1));
    }

    #[test]
    fn gcd_test() {
        assert_eq!(gcd(2 * 2 * 3 * 3, 2 * 3 * 5 * 7), 2 * 3);
    }

    #[test]
    fn lcm_test() {
        assert_eq!(lcm(2 * 2 * 3 * 3, 2 * 3 * 5 * 7), 2 * 2 * 3 * 3 * 5 * 7);
    }

    #[test]
    fn primes_test() {
        assert_eq!(
            Primes::new().iter().take(9).collect::<Vec<_>>(),
            vec![2, 3, 5, 7, 11, 13, 17, 19, 23]
        );
    }

    #[test]
    fn divisors_test() {
        assert_eq!(
            Primes::new()
                .factors_iter(2 * 2 * 3 * 7 * 7 * 7 * 13)
                .collect::<Vec<_>>(),
            vec![(2, 2), (3, 1), (7, 3), (13, 1)]
        );
    }

    #[test]
    fn num_divisors_test() {
        assert_eq!(
            Primes::new().num_divisors(2 * 2 * 3 * 7 * 7 * 7 * 13),
            3 * 2 * 4 * 2
        );
    }

    #[test]
    fn triangle_num_divisors_test() {
        let primes = Primes::new();
        let n = 99;
        assert_eq!(
            primes
                .triangle_num_factors_iter()
                .take(n)
                .collect::<Vec<_>>(),
            triangle_nums()
                .map(|t| (t, primes.num_divisors(t)))
                .take(n)
                .collect::<Vec<_>>(),
        );
    }
}

//! A Project Euler solver
use project_euler_solver::{solvers, Solver::*};

fn main() -> Result<(), std::io::Error> {
    println!("Welcome to the Project Euler solution calculator!");
    println!("Please type the numbers of the problems you wish to calculate the answer for.");
    let mut problem_nrs_str = String::new();
    std::io::stdin().read_line(&mut problem_nrs_str)?;
    let solvers = solvers();
    let (solved_nrs, unsolved_nrs) = problem_nrs_str
        .split_whitespace()
        .filter_map(|s| s.parse::<usize>().ok())
        .partition::<Vec<_>, _>(|n| solvers.contains_key(n));
    if solved_nrs.is_empty() {
        solvers.iter().for_each(|(&n, s_a)| match s_a {
            Unsigned(f, a) => print_solution(n, *f, *a),
            Signed(f, a) => print_solution(n, *f, *a),
        })
    } else {
        for n in solved_nrs {
            if let Some(s_a) = solvers.get(&n) {
                match s_a {
                    Unsigned(f, a) => print_solution(n, *f, *a),
                    Signed(f, a) => print_solution(n, *f, *a),
                }
            }
        }
    }
    if !unsolved_nrs.is_empty() {
        println!(
            "The solution to these problems is not available: {nrs} ",
            nrs = SpaceVec(unsolved_nrs)
        );
    }
    Ok(())
}

fn print_solution<T: std::cmp::PartialEq + std::fmt::Debug + std::fmt::Display>(
    n: usize,
    f: fn() -> T,
    answer: T,
) {
    let a = f();
    assert_eq!(answer, a);
    println!("The answer to problem {num} is: {ans}", num = n, ans = a,);
}

/// For outputting a vector's elements
struct SpaceVec<T: std::fmt::Display>(Vec<T>);
impl<T: std::fmt::Display> std::fmt::Display for SpaceVec<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        let mut output = String::new();
        self.0[0..self.0.len() - 1].iter().for_each(|e| {
            output.push_str(&e.to_string());
            output.push(' ');
        });
        output.push_str(&self.0[self.0.len() - 1].to_string());
        write!(f, "{}", output)
    }
}
